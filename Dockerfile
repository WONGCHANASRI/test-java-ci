FROM openjdk:8u111-jdk-alpine 
VOLUME /tmp 
ADD /target/demo-0.0.1-SNAPSHOT.jar app.jar 
ENTRYPOINT ["java","-Djava.security.egd=ﬁle:/dev/./urandom","-jar","/app.jar"]